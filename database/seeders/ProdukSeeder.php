<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Produk;
class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0;$i<100;$i++){
            Produk::create([
                'nama_produk'=>'nama_produk_'.$i,
                'kategori'=>'kategori_'.$i,
                'harga' => rand(1,20).'000' ,
                'keterangan'=>'keterangan_'.$i,
                'status'=>'status_'.$i]
            );
        }
    }
}
