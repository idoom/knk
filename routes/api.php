<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/produks', 'App\Http\Controllers\ProdukController@index');
Route::post('/produk/store', 'App\Http\Controllers\ProdukController@store');
Route::get('/produk/edit/{id}', 'App\Http\Controllers\ProdukController@getProduk');
Route::get('/produk/show/{id}', 'App\Http\Controllers\ProdukController@getProduk');
Route::put('/produk/update/{id}', 'App\Http\Controllers\ProdukController@update');
Route::delete('/produk/delete/{id}', 'App\Http\Controllers\ProdukController@delete'); 