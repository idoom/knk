<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
class ProdukController extends Controller
{
    //
    public function index()
    {
        $articles = Produk::all();
 
        return $articles->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'nama_produk' => 'required',
          'kategori' => 'required',
          'harga' => 'required',
          'keterangan' => 'required',
          'status' => 'required',
        ]);
 
        $project = Produk::create([
          'nama_produk' => $validatedData['nama_produk'],
          'kategori' => $validatedData['kategori'],
          'harga' => $validatedData['harga'],
          'keterangan' => $validatedData['keterangan'],
          'status' => $validatedData['status'],
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Produk created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getProduk($id) // for edit and show
    {
        $article = Produk::find($id);
 
        return $article->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'nama_produk' => 'required',
          'kategori' => 'required',
          'harga' => 'required',
          'keterangan' => 'required',
          'status' => 'required',
        ]);
 
        $article = Produk::find($id);
        $article->nama_produk = $validatedData['nama_produk'];
        $article->kategori = $validatedData['kategori'];
        $article->harga = $validatedData['harga'];
        $article->keterangan = $validatedData['keterangan'];
        $article->status = $validatedData['status'];
        $article->save();
 
        $msg = [
            'success' => true,
            'message' => 'Produk updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $article = Produk::find($id);
        if(!empty($article)){
            $article->delete();
            $msg = [
                'success' => true,
                'message' => 'Produk deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Produk deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
